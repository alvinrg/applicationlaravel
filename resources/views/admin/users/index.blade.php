@extends('layouts.admin')

@section('content')

    @if(Session::has('deleted_user'))
        <p class="bg-danger">{{session('deleted_user')}}</p>
    @endif

    <h1>Users</h1>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Photo</th>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Role</th>
                <th scope="col">Status</th>
                <th scope="col">Created</th>
                <th scope="col">Updated</th>
            </tr>
        </thead>
        <tbody>
        @if($users)
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td><img height="50" width="50" src="{{$user['photo']}}" alt="image"></td>
                    <td>{{$user['id']}}</td>
                    <td><a href="{{route('admin.users.edit', $user['id'])}}">{{$user['name']}}</a></td>
                    <td>{{$user['email']}}</td>
                    <td>{{$user['role']}}</td>
                    <td>{{$user['is_active']}}</td>
                    <td>{{$user['created_at']}}</td>
                    <td>{{$user['updated_at']}}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop