<?php

namespace App\Repository\Eloquent;

use App\Repository\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Photo;
use App\Role;
use App\Http\Requests\UsersRequest;
use App\Http\Requests\UsersEditRequest;
use App\Http\Resources\UserResource;
use App\Repository\Eloquent;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Collection;

class UserRepository
{
    /**
     * @var User
     */
     protected $user, $photo, $roles;

    /**
     * UserRepository constructor.
     *
     * @param User $user
     */
    public function __construct(User $user, Photo $photo, Role $roles)
    {
        $this->user = $user;
        $this->photo = $photo;
        $this->roles = $roles;
    }

    /**
     * function to get all data
     */
    public function getAll()
    {

        $collection = $this->user->all();
        return UserResource::collection($collection)->toArray(request());
    }

    /**
     * function to view Form User
     * @return $roles for user
     */
    public function getRoles()
    {

        return $this->roles->pluck('name','id')->all();;
    }

    /**
     * Function to find user by id
     */
    public function find($id)
    {
        return $this->user->findOrFail($id);
    }

    /**
    * @param $request
    * function to create user
    */
    public function createUser(UsersRequest $request)
    {
        $params = $request->all();
        $params['password'] = Hash::make($request->password);
        if($file = $request->file('photo_id')){
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = $this->photo->create(['file'=>$name]);
            $params['photo_id'] = $photo->id;
        }

        return  $this->user->create($params);

        // return $this->model->create($attributes);
    }

    /**
     * Function to update user
     */
    public function updateUser(UsersEditRequest $request, $id)
    {
        $user = $this->user->findOrFail($id);
        $input = $request->all();

        if($file = $request->file('photo_id')){
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = $this->photo->create(['file'=>$name]);
            $input['photo_id'] = $photo->id;
        }

        if($input['password'] == null){
            unset($input['password']);
        }else{
            $input['password'] = Hash::make($request->password);
        }

        return $user->update($input);
    }

    public function deleteUser($id)
    {
        $user = $this->user->findOrFail($id);
        unlink(public_path() . $user->photo->file);
        $user->delete();

        return Session::flash('deleted_user', 'The user has been deleted');;
    }

}

?>