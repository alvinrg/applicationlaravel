<?php

namespace App\Repository;


use Illuminate\Database\Eloquent\Model;

/**
* Interface UserRepositoryInterface
* @package App\Repositories
*/
interface UserRepositoryInterface
{
   /**
    * @param array $attributes
    * @return Model
    */
   public function create(array $attributes): Model;

   /**
    * @param $id
    * @return Model
    */
   public function find($id): ?Model;

   public function index(): Model;

   public function getAll(): Collection;
}

?>