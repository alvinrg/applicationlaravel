<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        $return = [
            'photo' => $this->photo ? $this->photo->file : 'https://placehold.it/400x400',
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role ? $this->role->name : 'User has no role',
            'is_active' => $this->is_active == 1 ? 'Active' : 'Not Active',
            'created_at' => $this->created_at->diffForHumans(),
            'updated_at' => $this->created_at->diffForHumans()
        ];

        return $return;
    }
}
