<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logout', 'Auth\LoginController@logout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/post/{id}', 'AdminPostsController@post')->name('home.post');

Route::get('/admin', function(){
    return view('admin.index');
})->name('admin');

Route::group(['middleware' => 'admin'], function () {
    //
    Route::get('admin/users', 'AdminUsersController@index')->name('admin.users.index');
    Route::post('admin/users', 'AdminUsersController@store')->name('admin.users.store');
    Route::get('admin/users/create', 'AdminUsersController@create')->name('admin.users.create');
    Route::get('admin/users/show', 'AdminUsersController@show')->name('admin.users.show');
    Route::get('admin/users/edit/{id}', 'AdminUsersController@edit')->name('admin.users.edit');
    Route::patch('admin/users/update/{id}', 'AdminUsersController@update')->name('admin.users.update');
    Route::delete('admin/users/{id}', 'AdminUsersController@destroy')->name('admin.users.delete');

    Route::get('admin/posts', 'AdminPostsController@index')->name('admin.posts.index');
    Route::get('admin/posts/create', 'AdminPostsController@create')->name('admin.posts.create');
    Route::post('admin/posts', 'AdminPostsController@store')->name('admin.posts.store');
    Route::get('admin/posts/edit/{id}', 'AdminPostsController@edit')->name('admin.posts.edit');
    Route::patch('admin/posts/update/{id}', 'AdminPostsController@update')->name('admin.posts.update');
    Route::delete('admin/posts/{id}', 'AdminPostsController@destroy')->name('admin.posts.destroy');

    Route::get('admin/categories', 'AdminCategoriesController@index')->name('admin.categories.index');
    Route::post('admin/categories', 'AdminCategoriesController@store')->name('admin.categories.store');
    Route::get('admin/categories/edit/{id}', 'AdminCategoriesController@edit')->name('admin.categories.edit');
    Route::patch('admin/categories/update/{id}', 'AdminCategoriesController@update')->name('admin.categories.update');
    Route::delete('admin/categories/{id}', 'AdminCategoriesController@destroy')->name('admin.categories.destroy');

    Route::get('admin/media', 'AdminMediasController@index')->name('admin.medias.index');
    Route::get('admin/media/create', 'AdminMediasController@create')->name('admin.medias.create');
    Route::post('admin/media', 'AdminMediasController@store')->name('admin.medias.store');
    Route::delete('admin/media/{id}', 'AdminMediasController@destroy')->name('admin.medias.destroy');

    Route::get('admin/comments', 'PostCommentsController@index')->name('admin.comments.index');
    Route::post('admin/comments', 'PostCommentsController@store')->name('admin.comments.post');
    Route::get('admin/comments/{id}', 'PostCommentsController@show')->name('admin.comments.show');
    Route::patch('admin/comments/update/{id}', 'PostCommentsController@update')->name('admin.comments.update');
    Route::delete('admin/comments/{id}', 'PostCommentsController@destroy')->name('admin.comments.delete');

    Route::get('admin/comment/replies', 'CommentRepliesController@index')->name('post.replies.index');
    Route::get('/admin/comment/replies/{id}', 'CommentRepliesController@show')->name('admin.comment.replies.show');
    Route::patch('/admin/comment/replies/{id}', 'CommentRepliesController@update')->name('admin.comment.replies.update');
    Route::delete('/admin/comment/replies/{id}', 'CommentRepliesController@destroy')->name('admin.comment.replies.destroy');

});

Route::group(['middleware' => 'auth'], function () {
    Route::post('comment/reply', 'CommentRepliesController@createReply')->name('post.replies.store');

});